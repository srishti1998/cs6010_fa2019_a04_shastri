# **SRISHTI SHASTRI**                        
# **ASSIGNMENT-04**

### **TABLE OF CONTENTS**

[Article 1- "Data Science: The Sexiest Job of 21st Century"](#)

[Article 2- "Data Science is Boring"](#)




## **Article 1- "Data Science: The Sexiest Job of 21st Century"**

> ### Summary

According to the article, a lot of companies want to hire Data Scientists because now they have to manage more data than ever before. The main objective is to find data scientists, attract them and then select the best one out of them. Moreover, if you want a better Data Scientist raising their salaries will be a better option. There are many ways to get a data scientist such as finding them in universities, finding them on LinkedIn, do not focus on coding only just ask them about their favorite analysis. Shortage of data scientists is a major issue and the main job of Data Scientists is to work on unstructured data not to work as consultants.  

> ### Quotes

1. “ Data Scientists don’t do well on short leash.”
2. “What Data Scientists do is make discoveries while swimming in data”
3. “Data never stop flowing”
4. “The demand from companies has been phenomenal”

> ### Questions

__Q1. What is the author's main point about Data Science?__

Ans-  The main point of the author is that there is a shortage of Data Science and it is a serious issue. The whole view of the article is what are the abilities and need of data scientists and how to find them.

__Q2. What does the article point out as the main skill/duty of a Data Scientist?**__

Ans- The main skills/duties of a Data Scientist:

1.	Good communication skills

2.	Good statistical and mathematical analysis of data

3.	Ability to present

__Q3. Are there parts of the article that you disagree with or are confused by?__

Ans-  I don’t think that I disagree with any point because yes Data Scientists are in demand and will be in demand as they will manage all the risks, present the data and as a result will put the company in the correct position.

## **Article 2- "Data Science is Boring"**

> ### Summary

In this article, the author tells about the conversation on the boring part of data science with his younger brother. The author portrays the difference between the expectation, the reality and how to deal with that. It is a lot more than just dealing with models. Whenever you come up with an idea then just discuss it with someone and perform experiments instead of giving up because of deadlines and workload. There might be a possibility that your code is correct but then also it is rejected because it is not the way they wanted. The reality is being a Data Scientist is not an easy job and there will be a lot of situations where you might feel frustrated and annoyed but you have to deal with that to stay.

> ### Quotes

 1. "Boringness in Data Science"
 2. "Keep-It-Simple-Stupid"
 3. "Breathe, Smile and Listen"
 4. "Just Execute"
 5. "Don't Wait, Ask for Help"

> ### Questions

__Q1. What is the author's main point about Data Science?__

Ans-  The main point of the author is that you should be fully prepared if you want to be Data Scientist because it is not only coding and dealing with machines there is a lot more than that. Moreover, you should know that Data Science in the Real World is very difficult and it can be frustrating and boring at the same time. So, just be ready to face challenges to be in the game.

__Q2. What does the article point out as the main skill/duty of a Data Scientist?__

Ans- The duties/skills of a Data Scientist are-

a) To come up with brilliant ideas.

b) To think smarter to deal with bad results.

c) Be concrete.

d) To do non-ML work as well.


__Q3. Are there parts of the article that you disagree with or are confused by?__

Ans- I don't disagree with or confused by any part of the article. I think it is correct that it is boring but one thing that I want to mention is you can make it interesting by doing the coping mechanism.

## **Comparision Between both Articles**

There is one thing common in both the articles that both the authors are talking about how data scientists should enhance their skills and what are the challenges they are facing in the real world. And the difference is in one article the author is talking about the shortage of Data Scientists and how different methods are planned to reach them. However, in another article, the author is telling about boringness in the field of Data Science and the measures to be taken to overcome the boringness. Both the authors have a different point of view about Data Science and I think that many companies need Data Scientists but it is tough to find them because there are not a lot of people with the combination of rare and powerful.  And no doubt it is boring but if you do your work with fun then you can find your way in that boring world also.
I agree more with the second article- “Data Science is Boring” because it is relatable in some way. If we keep data science aside and just think of any field then you will feel the same everywhere. I agree with the author’s coping mechanism and all the tips which a person should follow to be in the game.
